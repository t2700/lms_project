import React from 'react'
import { Line } from "react-chartjs-2";
import { Chart as ChartJS, registerables } from 'chart.js';
ChartJS.register(...registerables);

export function LineChartComponent() {
  let  yaxisdata =  ['Below Average', "Average","Above Average", "Good","Excellent"];
    
    const data = {
        labels: ["Mock1", "Mock2", "Mock3", "Mock4", "Mock5"],

        datasets: [
            {
                label: "First dataset",
                // data : ["average","Good","bad","excelent"],
                data : [0,1,3,4,2],
                fill: false,
                backgroundColor: "#FFFFFF",
                borderColor: "#FAA81D",
            },
        ]
    };
    const options = {
        plugins:{
            legend:{
                display:false
            }
        },
        scales: {
            x: {
                display: true,
                grid: {
                    display: false,
                },
                ticks:{
                    display: true,  
                }
            },
            y: {
                grid: {
                    display: false,
                },
                ticks:{
                    display: true,  
                    stepSize: 1,
                    // beginsAtZero:true,
                    callback: function(value, index, ticks) {
                        return yaxisdata[index]
                    }
                },
            }
        }
    }
    return (
        <div className="App">
            <Line data={data} width={250} height={50} options={options}/>
        </div>
    )
}
