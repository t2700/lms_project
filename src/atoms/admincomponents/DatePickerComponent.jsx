import { Stack, TextField } from '@mui/material'
import React,{useState} from 'react';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';


function DatePickerComponent() {
    const [value, setValue] = useState(null);

  return (
    <Stack  sx={{ width: 340 }} >
 <DatePicker
    label=""
    value={value}
    onChange={(newValue) => {
      setValue(newValue);
    }}
    renderInput={(params) => <TextField size="small" {...params} />}
  />
    </Stack>
   
  )
}

export default DatePickerComponent