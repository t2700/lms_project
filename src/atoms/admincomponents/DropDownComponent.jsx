import * as React from 'react';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

export default function DropDownComponent({singleDropData,handleSingleDrop,size,value,age,setAge,width,label}) {

 
  return (
    <FormControl  sx={{ width: {width} }} size={size}>
      <Select
        labelId="demo-select-small"
        id="demo-select-small"
        value={age}
        label={label}
        onChange={handleSingleDrop}
      >
        
        {singleDropData.map((ele,index)=>{
            return(  <MenuItem value={ele.value}>{ele.label}</MenuItem>)

        })}
        
      </Select>
    </FormControl>
  );
}
