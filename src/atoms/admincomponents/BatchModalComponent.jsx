import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import Typography from '@mui/material/Typography';
import { TextfieldComponent } from './TextfieldComponent';
import { Grid, InputLabel } from '@mui/material';
import DropDownComponent from './DropDownComponent';
import MultiDropdownComponent from './MultiDropdownComponent';
import DatePickerComponent from './DatePickerComponent';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
    },

}));

const BootstrapDialogTitle = (props) => {
    const { children, onClose, ...other } = props;


    return (
        <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
            {children}
            {onClose ? (
                <IconButton
                    aria-label="close"
                    onClick={onClose}
                    sx={{
                        position: 'absolute',
                        right: 8,
                        top: 8,
                        color: (theme) => theme.palette.grey[500],
                    }}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </DialogTitle>
    );
};

BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
};


export default function ModalComponent({ open, setOpen }) {
    const [age, setAge] = useState('');
    const [personName, setPersonName] = useState([]);
    const names = [
        'Oliver Hansen',
        'Van Henry',
        'April Tucker',
        'Ralph Hubbard',
        'Omar Alexander',
        'Carlos Abbott',
        'Miriam Wagner',
        'Bradley Wilkerson',
        'Virginia Andrews',
        'Kelly Snyder',
    ];
    const handleMultiChange = (event) => {
        debugger
        let {
            target: { value },
        } = event;
        setPersonName(
            // On autofill we get a stringified value.
            typeof value === 'string' ? value.split(',') : value,
        );
    };


    const handleClose = () => {
        setOpen(false);
    };
    // input
    const InputOnChange = () => {

    }
    //drop down
    let singleDropData = [
        { label: "One", value: "one" },
        { label: "Two", value: "two" },
        { label: "Three", value: "three" },
    ]
    const handleSingleDrop = (event) => {
        setAge(event.target.value);
    };
    return (
        <div>

            <BootstrapDialog
                onClose={handleClose}
                fullWidth
                aria-labelledby="customized-dialog-title"
                open={open}
            >
                <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose} sx={{ color: "#086288" }}>
                    Add new batch
                </BootstrapDialogTitle>
                <DialogContent dividers>
                    <Grid direction={{ xs: 'column', sm: 'column' }} sx={{ display: "flex", alignItems: "center", justifyContent: "center", }}>
                        <Grid sx={{ minWidth: "60%" }}>
                            <InputLabel>Batch Name</InputLabel>
                            <TextfieldComponent
                                variant="outlined"
                                inputOnChange={InputOnChange}
                                size='small'
                            />
                        </Grid>
                        <Grid mt={1}>
                            <InputLabel>Mentor Name</InputLabel>
                            <DropDownComponent
                                size="small"
                                singleDropData={singleDropData}
                                handleSingleDrop={handleSingleDrop}
                                age={age}
                                setAge={setAge}
                                width= {340}
                            />
                        </Grid>

                        <Grid mt={1}>
                            <InputLabel>Technologies</InputLabel>
                            <MultiDropdownComponent
                                names={names}
                                personName={personName}
                                setPersonName={setPersonName}
                                handleMultiChange={handleMultiChange}
                                size="small"
                                width={340}
                            />
                        </Grid>
                        <Grid mt={1}>
                            <InputLabel>Start Date</InputLabel>
                            <DatePickerComponent/>
                        </Grid>
                        <Grid mt={1}>
                            <InputLabel>End Date</InputLabel>
                            <DatePickerComponent />
                        </Grid>
                    </Grid>

                </DialogContent>
                <DialogActions>
                    <Button
                        sx={{ color: "#FFFFFF", borderColor: "#086288", backgroundColor: "#086288", ':hover': { border: "1px solid #086288", backgroundColor: "#086288" } }}
                        variant="contained"
                        onClick={handleClose}>
                        Create
                    </Button>
                </DialogActions>
            </BootstrapDialog>
        </div>
    );
}
