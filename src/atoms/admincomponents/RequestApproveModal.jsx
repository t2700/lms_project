import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import { Grid, InputLabel } from '@mui/material';
import DropDownComponent from './DropDownComponent';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },

}));

const BootstrapDialogTitle = (props) => {
  const { children, onClose, ...other } = props;


  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
};

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
};


export default function RequestApproveModal({ open, setOpen }) {
  const [age, setAge] = useState('');

  const handleClose = () => {
    setOpen(false);
  };

  //drop down
  let singleDropData = [
    { label: "One", value: "one" },
    { label: "Two", value: "two" },
    { label: "Three", value: "three" },
  ]
  const handleSingleDrop = (event) => {
    setAge(event.target.value);
  };
  return (
    <div>

      <BootstrapDialog
        onClose={handleClose}
        fullWidth
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose} sx={{ color: "#086288" }}>
          Change Status
        </BootstrapDialogTitle>
        <DialogContent dividers>
          <Grid direction={{ xs: 'column', sm: 'column' }} sx={{ display: "flex", alignItems: "center", justifyContent: "center", }}>

            <Grid mt={1}>
              <InputLabel>Batch Name</InputLabel>
              <DropDownComponent
                size="small"
                singleDropData={singleDropData}
                handleSingleDrop={handleSingleDrop}
                age={age}
                setAge={setAge}
                width= {340}
              />
            </Grid>

            <Grid mt={2}>
              <InputLabel>Batch ID</InputLabel>
              <DropDownComponent
                size="small"
                singleDropData={singleDropData}
                handleSingleDrop={handleSingleDrop}
                age={age}
                setAge={setAge}
                width= {340}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            sx={{ color: "#FFFFFF", borderColor: "#086288", backgroundColor: "#086288", ':hover': { border: "1px solid #086288", backgroundColor: "#086288" } }}
            variant="contained"
            onClick={handleClose}>
            Submit
          </Button>
        </DialogActions>
      </BootstrapDialog>
    </div>
  );
}
