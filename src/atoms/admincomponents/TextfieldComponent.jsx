import { TextField } from '@mui/material'
import React from 'react'

export function TextfieldComponent({variant,inputOnChange,size}) {
    

    return (
        <>
            <TextField id="outlined-basic" fullWidth size={size} label="" variant={variant} onChange={inputOnChange} sx={{border:"086288"}} />
        </>
    )
}
