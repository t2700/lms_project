import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import EditOutlinedIcon from '@mui/icons-material/EditOutlined';
import DeleteIcon from '@mui/icons-material/Delete';
import { Button, Checkbox } from '@mui/material';

export default function TableComponent({ rows, rowHeader,actions ,handleApprove,handleReject}) {
    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead bgcolor="lightgray">
                    <TableRow sx={{ fontWeight: "600" }}>
                        <TableCell padding="checkbox">
                            <Checkbox
                                color="primary"
                                // indeterminate={numSelected > 0 && numSelected < rowCount}
                                // checked={rowCount > 0 && numSelected === rowCount}
                                // onChange={onSelectAllClick}
                                inputProps={{
                                    'aria-label': 'select all desserts',
                                }}
                            />
                        </TableCell>
                        <TableCell align="center">No.</TableCell>
                        {rowHeader.map((val) => {
                            return (

                                <TableCell align="center">{val.label}</TableCell>
                            )
                        })}

                        <TableCell align="center">Action</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((val, ind) => {
                        debugger
                        return (
                            <>
                                <TableRow
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                    <TableCell padding="checkbox">
                                        <Checkbox
                                            color="primary"
                                            // indeterminate={numSelected > 0 && numSelected < rowCount}
                                            // checked={rowCount > 0 && numSelected === rowCount}
                                            // onChange={onSelectAllClick}
                                            inputProps={{
                                                'aria-label': 'select all desserts',
                                            }}
                                        />
                                    </TableCell>
                                    <TableCell align="center">{ind + 1}</TableCell>
                                    {rowHeader.map((ele, index) => {
                                        debugger
                                        return (
                                            <TableCell component="th" scope="row" align="center">
                                                {val[ele.id]}
                                            </TableCell>
                                        )
                                    })}
                                    { actions.includes("edit") || actions.includes("delete") ?(
                                            <TableCell align="center"><EditOutlinedIcon /><DeleteIcon /></TableCell>
                                        ):(
                                            <TableCell align="center"><Button variant="contained"sx={{marginRight:"11px",border:"#399d4d" ,color:"#399d4d" ,backgroundColor:"#def0e1",':hover': { border: "1px solid #399d4d", backgroundColor: "#def0e1" }}} onClick={handleApprove}>Approve</Button>{" "}
                                            <Button variant="contained" sx={{border:"#d93943",marginLeft:"2px", color:"#d93943" ,backgroundColor:"#fae5e6",':hover': { border: "1px solid #d93943", backgroundColor: "#fae5e6" }}} onClick={handleReject}>Reject</Button></TableCell>
                                        )
                                    }
                                   
                                </TableRow>
                            </>
                        )
                    })}
                </TableBody>
            </Table>
        </TableContainer>
    );
}
