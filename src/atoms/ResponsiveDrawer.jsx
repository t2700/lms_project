import React, { useEffect } from 'react';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import { Grid, Stack } from '@mui/material';
import batch from "../assests/batch.png";
import mentor from "../assests/mentor.png";
import request from "../assests/request.png";
import batch1 from "../assests/batch1.png";
import mentor1 from "../assests/mentor1.png";
import request1 from "../assests/request1.png";
import dbImg from "../assests/dbImg.png";
import dbImg1 from "../assests/dbImg1.png";
import { useLocation, Link } from 'react-router-dom';

const drawerWidth = 80;

export default function ResponsiveDrawer({mentorLogin}) {
    let location = useLocation();
    useEffect(() => {
        console.log(location.pathname)
    }, [location]);
    let drawerData ;
    if(mentorLogin){
        drawerData = [
            {
                activeImage: dbImg1,
                preImage: dbImg,
                text: "Dashboard",
                color: "Black",
                to: "/mentor/dashboard",
            },
            {
                activeImage: batch1,
                preImage: batch,
                text: "Batch",
                color: "Black",
                to: "/mentor/batch",
            },
        ]

    }else{
        drawerData = [
            {
                activeImage: batch1,
                preImage: batch,
                text: "Batch",
                color: "Black",
                to: "/home/batch",
            },
            {
                activeImage: mentor1,
                preImage: mentor,
                text: "Mentor",
                color: "Black",
                to: "/home/mentor",
            },
            {
                activeImage: request1,
                preImage: request,
                text: "Request",
                color: "Black",
                to: "/home/request",
            },
        ]
    }
  

    return (
        <Box sx={{ flexGrow: 1 }}>

            <Drawer
                sx={{
                    width: drawerWidth,
                    flexShrink: 0,
                    '& .MuiDrawer-paper': {
                        width: drawerWidth,
                        boxSizing: 'border-box',
                        top: "65px !important"
                    },
                }}
                variant="permanent"
                anchor="left"
            >
                <List >
                    {drawerData.map((val) => {
                        return (
                            <ListItem disablePadding >
                                <Link to={val.to} style={{textDecoration:"none"}}>
                                    <Stack sx={{ bgcolor: location.pathname === val.to ?"#086288":"#FFFFFF",display:"flex",alignContent:"center",justifyContent:"center",borderRadius:"5px",margin:"5px"}}>
                                    <ListItemButton > 
                                        <ListItemIcon >
                                            {location.pathname === val.to ?
                                                <Grid sx={{flexDirection: "column",display:"flex",alignItem:"center",justifyContent:"center"}}>
                                                    <img src={val.activeImage} alt="batch" width="36px" height="36px" />
                                                    <Typography variant='body1' sx={{ fontSize: "13px",color:"#FFFFFF" }}>
                                                        {val.text}
                                                    </Typography>
                                                </Grid> :
                                                <Grid sx={{flexDirection:"column",display:"flex",alignContent:"center",justifyContent:"center"}}>
                                                    <img src={val.preImage} alt="batch" width="36px" height="36px" />
                                                    <Typography variant='body1' sx={{ fontSize: "13px" }}>
                                                        {val.text}
                                                    </Typography>
                                                </Grid>}
                                        </ListItemIcon>
                                    </ListItemButton>
                                    </Stack>
                                </Link>
                            </ListItem>
                        )
                    })}
                  
                </List>
            </Drawer>

        </Box>
    );
}
