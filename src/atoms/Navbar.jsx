import React, { useState } from 'react';
import Box from '@mui/material/Box';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import { styled } from '@mui/material/styles';
import { Avatar, Button, Grid, InputBase, ListItem, ListItemAvatar, ListItemText, Menu, MenuItem, Typography } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import logo2 from "../assests/logo2.png";
import avtrimg from "../assests/avtrimg.png";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';


const drawerWidth = 70;

const UserBox = styled(Box)(({ theme }) => ({
    display: "flex",
    gap: "10px",
    alignItems: 'center',
    [theme.breakpoints.up("sm")]: {
        display: "block",
        
    },
    
    // top:"10%",
}));

export default function Navbar({ mentorLogin }) {

    const [open, setOpen] = useState(false)


    const Search = styled('div')(({ theme }) => ({
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: "#BCBCCB",
        '&:hover': {
            backgroundColor: "#BCBCCB",
        },

        margin: "0 auto",
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            margin: "0 auto",
            width: '50%',
        },
        opacity: "0.5"
    }));
    const SearchIconWrapper = styled('div')(({ theme }) => ({
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    }));
    const StyledInputBase = styled(InputBase)(({ theme }) => ({
        color: "black",
        '& .MuiInputBase-input': {
            padding: theme.spacing(1, 1, 1, 0),
            // vertical padding + font size from searchIcon
            paddingLeft: `calc(1em + ${theme.spacing(4)})`,
            transition: theme.transitions.create('width'),
            width: '100%',
            [theme.breakpoints.up('md')]: {
                width: '34ch',

            },
            marginLeft: "-2rem"
        },
    }));


    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="fixed" sx={{ bgcolor: "#FFFFFF" }}>
                <Toolbar>
                    <Grid sx={{ display: { xs: "block", sm: "block" } }}>
                        <img src={logo2} alt="logo" width="188px" height="54px" />
                    </Grid >
                    <Box sx={{ flexGrow: 1, display: { xs: "none", sm: "block" } }}>
                        <Search>
                            <SearchIconWrapper>
                                <SearchIcon sx={{ color: "gray" }} />
                            </SearchIconWrapper>
                            <StyledInputBase
                                sx={{ marginLeft: "40px" }}
                                placeholder="Search Mentor / Employee"
                                inputProps={{ 'aria-label': 'search' }}
                            />
                        </Search>
                    </Box>
                    {mentorLogin ?
                        <Box sx={{ display: { xs: "flex", md: "flex" }, margin: "0 auto" }}>
                             <UserBox onClick={(e) => setOpen(true)}>
                             <Avatar src={avtrimg} alt="Remy Sharp" />
                             </UserBox>
                             <UserBox onClick={(e) => setOpen(true)}>
                           
                                <Button
                                    sx={{ color: "black" }}
                                    endIcon={<ExpandMoreIcon />}
                                >
                                    Shalini
                                </Button>
                            </UserBox>
                        </Box> :
                        <Box sx={{ display: { xs: "flex", md: "flex" }, margin: "0 auto" }}>
                            <Button variant="outlined" sx={{ color: "#086288", borderColor: "#086288" }}>Logout </Button>
                        </Box>}
                </Toolbar>
            </AppBar>
            <Menu
                id="basic-menu"
                open={open}
               
                onClose={() => setOpen(false)}
                anchorOrigin={{
                    vertical: "top",
                    horizontal: "right"
                }}
                transformOrigin={{
                    vertical: "top",
                    horizontal: 'right'
                }}
            >
                <MenuItem sx={{fontSize:"13px"}} onClick={() => setOpen(false)}>Profile</MenuItem>
                <MenuItem  sx={{fontSize:"13px"}} onClick={() => setOpen(false)}>Change Password</MenuItem>
                <MenuItem  sx={{fontSize:"13px"}} onClick={() => setOpen(false)}>Logout</MenuItem>
            </Menu>

        </Box>
    );
}
