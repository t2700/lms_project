import React from 'react'
import { Bar } from "react-chartjs-2";
import { Chart as ChartJS, registerables } from 'chart.js';
ChartJS.register(...registerables);

export function BarChartComponent({barbgColor,bar_data,bar_label,axis,indexAxis}) {
    
  const data = {
    labels: bar_label,
    datasets: [{
      label: 'My First Dataset',
      data:bar_data,
        backgroundColor: barbgColor,
      hoverOffset: 20,
      borderRadius:10,
      
    }],
    axis:axis,
    
  };
    const options = {
        plugins:{
            legend:{
                display:false,
                position:'right',
                labels:{
                    usePointStyle:true
                }
            }
        },
        maintainAspectRatio: false,
        indexAxis: indexAxis,
    
        scales: {
            x: {
                display: false,
                grid: {
                    display: false,
                },
                ticks:{
                    display: false,  
                }
            },
            y: {
                display: true,
                grid: {
                    display: false,
                },
                ticks:{
                    display: true,  
                  
                },
            }
        }
    }
    return (
        <div className="App">
            <Bar data={data} options={options}/>
        </div>
    )
}
