import React from 'react'
import { Pie } from "react-chartjs-2";
import { Chart as ChartJS, registerables } from 'chart.js';
ChartJS.register(...registerables);

export function PieChartComponent({genderbgColor,gender_data,gender_label,cutout}) {
    
  const data = {
    labels: gender_label,
    datasets: [{
      label: 'My First Dataset',
      data:gender_data,
        backgroundColor: genderbgColor,
      hoverOffset: 20
    }],
    
  };
    const options = {
        plugins:{
            legend:{
                display:true,
                position:'right',
                labels:{
                    usePointStyle:true
                }
            }
        },
        cutout:cutout,
        maintainAspectRatio: false,
      
        scales: {
            x: {
                display: false,
                grid: {
                    display: false,
                },
                ticks:{
                    display: false,  
                }
            },
            y: {
                display: false,
                grid: {
                    display: false,
                },
                ticks:{
                    display: false,  
                  
                },
            }
        }
    }
    return (
        <div className="App">
            <Pie data={data} options={options}/>
        </div>
    )
}
