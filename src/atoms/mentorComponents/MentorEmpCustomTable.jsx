import React, { useState } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Button, Checkbox } from '@mui/material';
import ArrowForwardIosOutlinedIcon from '@mui/icons-material/ArrowForwardIosOutlined';
import WarningOutlinedIcon from '@mui/icons-material/WarningOutlined';
import ModalDialogCommon from './ModalDialogCommon';
import MenuItemComponent from './MenuItemComponent';
import MockRatingModal from './MockRatingModal';

export default function MentorEmpCustomTable({ rows, rowHeader, batchStrength, onEditClick, empList }) {
    const [anchorEl, setAnchorEl] = useState(null);
    const [modalOpen, setModalOpen] = useState(false);
    const [mockRatingOpen, setMockRatingOpen] = useState(false);

    const handleClick = (
        event) => {
        setAnchorEl(event.currentTarget);
    };

    let menudata = [
        {
            title: "Initial Strength",
            color: "#086288",
            total: "100"
        },
        {
            title: "Dropout",
            color: "#EC3E66",
            total: "10"
        },
        {
            title: "Terminated",
            color: "#EC3E66",
            total: "10"
        },
        {
            title: "Absconding",
            color: "#EC3E66",
            total: "10"
        },
        {
            title: "Present Strength",
            color: "#02B91B",
            total: "70"
        }
    ]
    return (
        <>
         {mockRatingOpen && (
        <MockRatingModal open={mockRatingOpen} setOpen={setMockRatingOpen}/>
      )}
            {modalOpen && (
                <ModalDialogCommon open={modalOpen} setOpen={setModalOpen} />
            )}
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead bgcolor="lightgray">
                        <TableRow sx={{ fontWeight: "600" }}>
                            <TableCell padding="checkbox">
                                <Checkbox
                                    color="primary"
                                    inputProps={{
                                        'aria-label': 'select all desserts',
                                    }}
                                />
                            </TableCell>
                            <TableCell align="center">No.</TableCell>
                            {rowHeader.map((val) => {
                                return (

                                    <TableCell align="center">{val.label}</TableCell>
                                )
                            })}
                            {batchStrength && (
                                <>
                                    <TableCell align="center">Batch Strength</TableCell>
                                    <TableCell align="left"></TableCell>
                                    <TableCell align="left"></TableCell>
                                </>
                            )}
                            {empList && (
                                <>
                             <TableCell align="left"></TableCell>
                            <TableCell align="left"></TableCell>
                            </>
                            )}

                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map((val, ind) => {
                            return (
                                <>
                                    <TableRow
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                        <TableCell padding="checkbox">
                                            <Checkbox
                                                color="primary"
                                                inputProps={{
                                                    'aria-label': 'select all desserts',
                                                }}
                                            />
                                        </TableCell>
                                        <TableCell align="center">{ind + 1}</TableCell>
                                        {rowHeader.map((ele, index) => {
                                            debugger
                                            return (
                                                <TableCell component="th" scope="row" align="center">
                                                    {val[ele.id]}
                                                </TableCell>
                                            )
                                        })}
                                        {batchStrength && (
                                            <>
                                                <TableCell align="center"><WarningOutlinedIcon sx={{ fontSize: "40px", color: "#C9A805" }} onMouseOver={handleClick} /></TableCell>
                                                <TableCell align="left"><Button onClick={() => setModalOpen(true)} sx={{ color: " #086288", ":hover": { backgroundColor: "#086288", color: "#FFFFFF" } }}>Attendance</Button></TableCell>
                                                <TableCell align="left"><ArrowForwardIosOutlinedIcon sx={{ fontSize: "17px" }} onClick={() => { onEditClick(val.batchId) }} /></TableCell>
                                            </>)}
                                        {empList && (
                                            <>
                                            <TableCell align="left"><Button onClick={() => setMockRatingOpen(true)} sx={{ color: " #086288", ":hover": { backgroundColor: "#086288", color: "#FFFFFF" } }}>Give Rating</Button></TableCell>
                                            <TableCell align="left"><ArrowForwardIosOutlinedIcon sx={{ fontSize: "17px" ,marginLeft:"-95%"}} onClick={() => { debugger; onEditClick(val.empID) }} /></TableCell>
                                            </>
                                        )}
                                    </TableRow>
                                </>
                            )
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
            <MenuItemComponent menudata={menudata} openmenu={Boolean(anchorEl)} anchorEl={anchorEl} setAnchorEl={setAnchorEl} />

        </>
    );
}
