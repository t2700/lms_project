import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import SwitchComponent from './SwitchComponent';


export default function SimpleTable({ rows, rowHeader }) {
    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead bgcolor="lightgray">
                    <TableRow sx={{ fontWeight: "600" }}>
                        
                        <TableCell align="center">No.</TableCell>
                        {rowHeader.map((val) => {
                            return (

                                <TableCell align="center">{val.label}</TableCell>
                            )
                        })}

                        <TableCell align="center">Attendance</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((val, ind) => {
                        debugger
                        return (
                            <>
                                <TableRow
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                   
                                    <TableCell align="center">{ind + 1}</TableCell>
                                    {rowHeader.map((ele, index) => {
                                        debugger
                                        return (
                                            <TableCell component="th" scope="row" align="center">
                                                {val[ele.id]}
                                            </TableCell>
                                        )
                                    })}
                                  
                                    <TableCell align="center" sx={{display:"flex",justifyContent:"space-evenly"}}><SwitchComponent label="M" bgcolor="#13BC07"/><span><SwitchComponent label="N" bgcolor="#CA0202"/></span></TableCell>
                                   
                                </TableRow>
                            </>
                        )
                    })}
                </TableBody>
            </Table>
        </TableContainer>
    );
}
