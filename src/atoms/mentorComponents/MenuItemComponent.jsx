import { Menu, MenuItem, Typography } from '@mui/material'
import React from 'react'

function MenuItemComponent({menudata,openmenu,anchorEl, setAnchorEl,width,handleClose,handleMenuClick}) {

   
  return (
   
    <Menu
    anchorEl={anchorEl}
    id="account-menu"
    open={openmenu}
    onClose={handleClose}
    onClick={handleClose}
    PaperProps={{
        elevation: 0,

        sx: {
            overflow: 'visible',
            width: {width},
            border: "1px solid #086288",
            filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
            mt: 1.5,
            '& .MuiAvatar-root': {
                width: 32,
                height: 32,
                ml: -0.5,
                mr: 1,
                border: "1px solid #086288",
            },
            '&:before': {
                content: '""',
                display: 'block',
                position: 'absolute',
                top: 0,
                right: 14,
                width: 10,
                height: 10,

                bgcolor: 'background.paper',
                transform: 'translateY(-50%) rotate(45deg)',
                zIndex: 0,
            },
        },
    }}
    transformOrigin={{ horizontal: 'right', vertical: 'top' }}
    anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
>
    {menudata.map((val) => {
        return (
            <MenuItem sx={{ display: "flex", justifyContent: "space-between" }}>
                <Typography variant="p" sx={{ fontWeight: 600 }}  onClick={handleMenuClick}>{val.title}</Typography>
                <Typography color={val.color}>{val.total}</Typography>
            </MenuItem>
        )
    })}

</Menu>
  )
}

export default MenuItemComponent