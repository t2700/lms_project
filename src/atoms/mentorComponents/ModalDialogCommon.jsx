import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import PropTypes from 'prop-types';
import SimpleTable from './SimpleTable';
import { Typography } from '@mui/material';

export default function ModalDialogCommon({ open, setOpen }) {
    //   const [open, setOpen] = React.useState(false);
    const [fullWidth, setFullWidth] = React.useState(true);
    const [maxWidth, setMaxWidth] = React.useState('lg');

    const handleClose = () => {
        setOpen(false);
    };

    const BootstrapDialogTitle = (props) => {
        const { children, onClose, ...other } = props;

        return (
            <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
                {children}
                {onClose ? (
                    <IconButton
                        aria-label="close"
                        onClick={onClose}
                        sx={{
                            position: 'absolute',
                            right: 8,
                            top: 8,
                            color: (theme) => theme.palette.grey[500],
                        }}
                    >
                        <CloseIcon />
                    </IconButton>
                ) : null}
            </DialogTitle>
        );
    };

    BootstrapDialogTitle.propTypes = {
        children: PropTypes.node,
        onClose: PropTypes.func.isRequired,
    };
    let batchHeader =[
        {label:"Employee ID", id:"empId"},
        {label:"Employee Name", id:"empName"},
    ]
    let rowsBatch=[{
        empId:"#15422252",
        empName: "abc",
    },{
        empId:"#434433",
        empName: "xyz",
    }];


    return (
        <React.Fragment>

            <Dialog
                fullWidth={fullWidth}
                maxWidth={maxWidth}
                maxHeight="400px"
                open={open}
                onClose={handleClose}
            >
                <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
                    <Typography variant="h6" sx={{color:"#086288"}}> Attendance for (20 Mar 2022)</Typography>
               
                </BootstrapDialogTitle>        
                <DialogContent dividers style={{ height: '500px' }}>
                   
                    <SimpleTable rows={rowsBatch} rowHeader={batchHeader} />
                    
                </DialogContent>
                <DialogActions >
                    <Button variant="contained"onClick={handleClose} sx={{backgroundColor:"#086288",":hover": {backgroundColor:"#086288",color:"#FFFFFF"}}}>Submit</Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>
    );
}
