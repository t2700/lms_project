import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import { TextfieldComponent } from '../admincomponents/TextfieldComponent';
import { Grid, InputAdornment, InputLabel, OutlinedInput } from '@mui/material';
import MultiDropdownComponent from '../admincomponents/MultiDropdownComponent';
import { Visibility, VisibilityOff } from '@mui/icons-material';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
    },
}));

const BootstrapDialogTitle = (props) => {
    const { children, onClose, ...other } = props;

    return (
        <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
            {children}
            {onClose ? (
                <IconButton
                    aria-label="close"
                    onClick={onClose}
                    sx={{
                        position: 'absolute',
                        right: 8,
                        top: 8,
                        color: (theme) => theme.palette.grey[500],
                    }}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </DialogTitle>
    );
};

BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
};


export default function MentorModal({ open, setOpen }) {
  
    const handleClose = () => {
        setOpen(false);
    };
    
  
    const [values, setValues] = useState({
        showPassword: false,
        active: true,
        idErr: false,
        pwdErr: false
    });
    const [userdata, setUserData] = useState({ reenter: "", password: "",reset:"" });


    const handleClickShowPassword = () => {
        setValues({
            ...values,
            showPassword: !values.showPassword,
        });
    };

    return (
        <div>
            <BootstrapDialog
                onClose={handleClose}
                fullWidth
                aria-labelledby="customized-dialog-title"
                open={open}
            >
                <BootstrapDialogTitle sx={{font:"normal normal 600 18px/24px Open Sans", color: "#086288", display: "flex", justifyContent: "center", textAlign: "center" }}>
                    Reset your password <br /> to continue
                </BootstrapDialogTitle>

                <DialogContent >
                    <Grid direction={{ xs: 'column', sm: 'column' }} sx={{ display: "flex", alignItems: "center", justifyContent: "center", }}>
                        <Grid sx={{ minWidth: "60%" }}>

                            <OutlinedInput
                                id="outlined-adornment-password"
                                type={values.showPassword ? 'text' : 'password'}
                                error={values.pwdErr}
                                helperText={values.pwdErr ? "Please enter Password" : ""}
                                value={userdata.password}
                                placeholder="Enter existing password"
                                onChange={(event) => { setUserData({ ...userdata, password: event.target.value }) }}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            edge="end"
                                        >
                                            {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                        </IconButton>
                                    </InputAdornment>
                                }
                                label=""
                                size="small"
                                fullWidth
                                sx={{ backgroundColor: "white", }}
                            />
                        </Grid>
                        <Grid mt={2} sx={{ minWidth: "60%" }}>

                            <OutlinedInput
                                id="outlined-adornment-password"
                                type={values.showPassword ? 'text' : 'password'}
                                error={values.pwdErr}
                                helperText={values.pwdErr ? "Please enter Password" : ""}
                                value={userdata.reset}
                                placeholder="Enter New Password (Must be at least 6 characters)"
                                onChange={(event) => { setUserData({ ...userdata, reset: event.target.value }) }}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            edge="end"
                                        >
                                            {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                        </IconButton>
                                    </InputAdornment>
                                }
                                label=""
                                size="small"
                                fullWidth
                                sx={{ backgroundColor: "white", }}
                            />
                        </Grid>
                        <Grid mt={2} sx={{ minWidth: "60%" }}>

                            <OutlinedInput
                                id="outlined-adornment-password"
                                type={values.showPassword ? 'text' : 'password'}
                                error={values.pwdErr}
                                helperText={values.pwdErr ? "Please enter Password" : ""}
                                value={userdata.reenter}
                                placeholder="Re-enter Password"
                                onChange={(event) => { setUserData({ ...userdata, reenter: event.target.value }) }}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            edge="end"
                                        >
                                            {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                        </IconButton>
                                    </InputAdornment>
                                }
                                label=""
                                size="small"
                                fullWidth
                                sx={{ backgroundColor: "white", }}
                            />
                        </Grid>

                    </Grid>
                </DialogContent>
                <DialogActions sx={{ display: "flex", justifyContent: "center" }}>
                    <Button
                        sx={{ color: "#FFFFFF", borderColor: "#086288", backgroundColor: "#086288", ':hover': { border: "1px solid #086288", backgroundColor: "#086288" } }}
                        variant="contained"
                        onClick={handleClose}>
                        Change
                    </Button>
                </DialogActions>
            </BootstrapDialog>
        </div>
    );
}
