import React, { useState } from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import PropTypes from 'prop-types';
import { styled } from '@mui/material/styles';
import { Grid, InputLabel, Paper, TextField, Typography } from '@mui/material';
import { Box } from '@mui/system';
import DropDownComponent from '../admincomponents/DropDownComponent';
import { TextfieldComponent } from '../admincomponents/TextfieldComponent';
import MultiDropdownComponent from '../admincomponents/MultiDropdownComponent';

export default function MockRatingModal({ open, setOpen }) {
    const [age, setAge] = useState('');
    const [fullWidth, setFullWidth] = React.useState(true);
    const [maxWidth, setMaxWidth] = React.useState('md');

    const handleClose = () => {
        setOpen(false);
    };

    const BootstrapDialogTitle = (props) => {
        const { children, onClose, ...other } = props;

        return (
            <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
                {children}
                {onClose ? (
                    <IconButton
                        aria-label="close"
                        onClick={onClose}
                        sx={{
                            position: 'absolute',
                            right: 8,
                            top: 8,
                            color: (theme) => theme.palette.grey[500],
                        }}
                    >
                        <CloseIcon />
                    </IconButton>
                ) : null}
            </DialogTitle>
        );
    };

    BootstrapDialogTitle.propTypes = {
        children: PropTypes.node,
        onClose: PropTypes.func.isRequired,
    };

    //drop down
    let singleDropData = [
        { label: "One", value: "one" },
        { label: "Two", value: "two" },
        { label: "Three", value: "three" },
    ]
    const handleSingleDrop = (event) => {
        setAge(event.target.value);
    };
    // input
    const InputOnChange = () => {
    }
    //multi drop
    const [personName, setPersonName] = useState([]);
    const names = [
        'Oliver Hansen',
        'Van Henry',
        'April Tucker',
        'Ralph Hubbard',
        'Omar Alexander',
        'Carlos Abbott',
        'Miriam Wagner',
        'Bradley Wilkerson',
        'Virginia Andrews',
        'Kelly Snyder',
    ];
    const handleMultiChange = (event) => {
        debugger
        let {
            target: { value },
        } = event;
        setPersonName(
            // On autofill we get a stringified value.
            typeof value === 'string' ? value.split(',') : value,
        );
    };
    return (
        <React.Fragment>

            <Dialog
                fullWidth={fullWidth}
                maxWidth={maxWidth}
                open={open}
                onClose={handleClose}
            >
                <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
                    <Typography variant="h6" sx={{ color: "#086288" }}>Mock Rating</Typography>

                </BootstrapDialogTitle>
                <DialogContent dividers style={{ height: '380px' }}>

                    <Box sx={{ flexGrow: 1 }} >
                        <Grid container spacing={2}>
                            <Grid item xs={6}>

                                <InputLabel>Mock Type</InputLabel>
                                <DropDownComponent
                                    size="small"
                                    singleDropData={singleDropData}
                                    handleSingleDrop={handleSingleDrop}
                                    age={age}
                                    setAge={setAge}
                                    width={420}
                                />

                            </Grid>
                            <Grid item xs={6}>
                                <InputLabel>Mock taken by</InputLabel>

                                <TextfieldComponent
                                    variant="outlined"
                                    inputOnChange={InputOnChange}
                                    size='small'
                                />

                            </Grid>
                            <Grid item xs={6}>
                                <InputLabel>Technologies</InputLabel>
                                <MultiDropdownComponent
                                    names={names}
                                    personName={personName}
                                    setPersonName={setPersonName}
                                    handleMultiChange={handleMultiChange}
                                    size="small"
                                    width={420}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <InputLabel>Practical Knowledge (out of 100)<span style={{color:"red"}}>*</span></InputLabel>

                                <TextfieldComponent
                                    variant="outlined"
                                    inputOnChange={InputOnChange}
                                    size='small'
                                />
                            </Grid> <Grid item xs={6}>
                                <InputLabel>Theoretical Knowledge (out of 100)<span style={{color:"red"}}>*</span></InputLabel>

                                <TextfieldComponent
                                    variant="outlined"
                                    inputOnChange={InputOnChange}
                                    size='small'
                                />
                            </Grid>
                            
                            <Grid item xs={6}>

                                <InputLabel>Overall Feedback((Practical + Theoretical)1/2)<span style={{color:"red"}}>*</span></InputLabel>
                                <DropDownComponent
                                    size="small"
                                    singleDropData={singleDropData}
                                    handleSingleDrop={handleSingleDrop}
                                    age={age}
                                    setAge={setAge}
                                    width={420}
                                />

                            </Grid>
                            <Grid item xs={12}>
                            <InputLabel>Detailed Feedback <span style={{color:"red"}}>*</span></InputLabel>
                            <TextField
                                id="outlined-multiline-static"
                                label=""
                                multiline
                                fullWidth
                                rows={3}
                                inputOnChange={InputOnChange}
                            />
                            </Grid>
                        </Grid>
                    </Box>
                </DialogContent>
                <DialogActions >
                    <Button variant="contained" onClick={handleClose} sx={{ backgroundColor: "#086288", ":hover": { backgroundColor: "#086288", color: "#FFFFFF" } }}>Submit</Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>
    );
}
