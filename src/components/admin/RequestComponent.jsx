import { Box, Breadcrumbs, Button, Grid, InputBase, Paper, Stack, Typography } from '@mui/material'
import React, { useState } from 'react'
import Navbar from '../../atoms/Navbar'
import ResponsiveDrawer from '../../atoms/ResponsiveDrawer'
import Link from '@mui/material/Link';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import { styled } from '@mui/material/styles';
import RequestModal from '../../atoms/admincomponents/RequestApproveModal';
import SearchIcon from '@mui/icons-material/Search';
import TableComponent from '../../atoms/admincomponents/TableComponent';
import RequestApproveModal from '../../atoms/admincomponents/RequestApproveModal';
import RequestRejectModal from '../../atoms/admincomponents/RequestRejectModal';


export function RequestComponent() {
    const [open, setOpen] = useState(false);
    const [approve, setApprove] = useState(false);
    const [reject, setReject] = useState(false);
  

    const breadcrumbs = [
        <Link
            underline="hover"
            key="2"
            color="inherit"
        >
            Home
        </Link>,
        <Typography key="3" color="text.primary">
            Request list
        </Typography>,
    ];
    const Item = styled(Paper)(({ theme }) => ({
        backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
        ...theme.typography.body2,
        padding: theme.spacing(1),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }));
    const Search = styled('div')(({ theme }) => ({
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: "#BCBCCB",
        '&:hover': {
            backgroundColor: "#BCBCCB",
        },

        margin: "0 auto",
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: "75%",
            width: '25%',
        },
        opacity: "0.5"
    }));
    const SearchIconWrapper = styled('div')(({ theme }) => ({
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    }));
    const StyledInputBase = styled(InputBase)(({ theme }) => ({
        color: "black",
        '& .MuiInputBase-input': {
            padding: theme.spacing(1, 1, 1, 0),
            // vertical padding + font size from searchIcon
            paddingLeft: `calc(1em + ${theme.spacing(4)})`,
            transition: theme.transitions.create('width'),
            width: '100%',
            [theme.breakpoints.up('md')]: {
                width: '34ch',

            },
            marginLeft: "-2rem"
        },
    }));
    let batchHeader = [
        { label: "Employee ID", id: "empId" },
        { label: "Employee Name", id: "empName" },
        { label: "YOP", id: "yop" },
        { label: "Percentage", id: "percentage" },
        { label: "Experience", id: "experience" },
        { label: "Contact No.", id: "contact" },
    ]
    let rowsBatch = [{
        empName: "yashaswini",
        empId: "#15422252",
        yop: "2022",
        percentage: "85%",
        experience: "Fresher",
        contact: "9900887766",
    }];

   
    return (
        <Box sx={{ width: '100%', bgcolor: "#f9f9f9" }}>
            {approve && (
                <RequestApproveModal open={approve} setOpen={setApprove} />
            )}
             {reject && (
                <RequestRejectModal open={reject} setOpen={setReject} label="Reason for Rejection" />
            )}
            <Grid container rowSpacing={5}>
                <Grid item xs={12} md={12} lg={12} sm={12}>
                    <Item>
                        <Navbar />
                    </Item>
                </Grid>
                <Grid item xs={3} sm={1.5} lg={1} md={1} xl={1} mt={5}>

                    <ResponsiveDrawer />
                </Grid>
                <Grid item xs={9} sm={10} lg={11} md={11} xl={11} mt={5}>
                    <Breadcrumbs
                        separator={<NavigateNextIcon fontSize="small" />}
                        aria-label="breadcrumb"
                    >
                        {breadcrumbs}
                    </Breadcrumbs>
                    <Grid container direction="row" rowSpacing={5} columnSpacing={{ xs: 1, sm: 2, md: 2 }}>
                        <Grid item xs={12} sm={12} bgcolor="#FFFFFF" sx={{ margin: "5% 2% 0% 0%" }}>
                            <Stack direction={{ xs: 'row', sm: 'row' }} sx={{ paddingBottom: "20px" }}
                                spacing={{ xs: 1, sm: 2, md: 4 }}>

                                <Grid sx={{ display: { xs: "block", sm: "block" }, color: "#faa81d" }}>
                                    Request list
                                </Grid >
                                <Box sx={{ flexGrow: 1, display: { xs: "block", sm: "block" } }}>
                                    <Search>
                                        <SearchIconWrapper>
                                            <SearchIcon sx={{ color: "gray" }} />
                                        </SearchIconWrapper>
                                        <StyledInputBase
                                            sx={{ marginLeft: "40px" }}
                                            placeholder="Search"
                                            inputProps={{ 'aria-label': 'search' }}
                                        />
                                    </Search>
                                </Box>
                            </Stack>
                            <TableComponent rows={rowsBatch}
                             rowHeader={batchHeader}
                              actions={["approve","reject"]} 
                              handleApprove={()=> setApprove(true)}
                              handleReject={()=>setReject(true)}
                              />
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Box>
    )
}
