import { Breadcrumbs, Button, Chip, Grid, InputBase, Paper, Stack, Typography } from '@mui/material'
import React,{useState} from 'react'
import Navbar from '../../atoms/Navbar'
import ResponsiveDrawer from '../../atoms/ResponsiveDrawer'
import Link from '@mui/material/Link';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import { Box } from '@mui/system';
import { styled } from '@mui/material/styles';
import TableComponent from '../../atoms/admincomponents/TableComponent';
import SearchIcon from '@mui/icons-material/Search';
import ModalComponent from '../../atoms/admincomponents/BatchModalComponent';


function BatchComponent() {
  const [open, setOpen] = useState(false);
  const breadcrumbs = [
    <Link
      underline="hover"
      key="2"
      color="inherit"
    >
      Home
    </Link>,
    <Typography key="3" color="text.primary">
      Batch
    </Typography>,
  ];

  const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }));

  const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: "#BCBCCB",
    '&:hover': {
      backgroundColor: "#BCBCCB",
    },

    margin: "0 auto",
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: "75%",
      width: '25%',
    },
    opacity: "0.5"
  }));
  const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  }));
  const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: "black",
    '& .MuiInputBase-input': {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: '34ch',

      },
      marginLeft: "-2rem"
    },
  }));
  const getChipComponent = (data)=>{
    // debugger
   return data.map((ele)=>{
      return(  <Chip label={ele} sx={{bgcolor:"#086288",color:"#FFFFFF"}}/>)
    }
       
    )
   
  }
  let batchHeader =[
    {label:"Batch ID", id:"batchId"},
    {label:"Mentor Name", id:"mentor"},
    {label:"Batch Name", id:"batchName"},
    {label:"Technologies", id:"technologies"},
    {label:"Start Date", id:"startDate"},
    {label:"End Date", id:"endDate"},
    {label:"Status", id:"status"},
  ]
let rowsBatch=[{
  mentor:"yashaswini",
  batchId:"#15422252",
  batchName: "abc",
  technologies:getChipComponent([
    "Node.JS",
    "NestJs",
    "Express",
    "React",
    "Jaavascript",
    "Angular"
  ]),
  endDate:"17-08-2022",
  status:"In progress",
  startDate:"14-07-2022",
}];
const handleNewbatch = ()=>{
  setOpen(true)
}
  return (
    <Box sx={{ width: '100%', bgcolor: "#f9f9f9" }}>
      {open && (
        <ModalComponent open={open} setOpen={setOpen}/>
      )}
      <Grid container rowSpacing={5}>
        <Grid item xs={12} md={12} lg={12} sm={12}>
          <Item>
            <Navbar />
          </Item>
        </Grid>
        <Grid item xs={3} sm={1.5} lg={1} md={1} xl={1} mt={5}>

          <ResponsiveDrawer />
        </Grid>
        <Grid item xs={9} sm={10} lg={11} md={11} xl={11} mt={5}>
          <Breadcrumbs
            separator={<NavigateNextIcon fontSize="small" />}
            aria-label="breadcrumb"
          >
            {breadcrumbs}
          </Breadcrumbs>
          <Grid container direction="row" rowSpacing={5} columnSpacing={{ xs: 1, sm: 2, md: 2 }}>
            <Grid item xs={12} sm={12}  bgcolor="#FFFFFF" sx={{margin:"5% 2% 0% 0%"}}>
              <Stack direction={{ xs: 'row', sm: 'row'}} sx={{paddingBottom:"20px"}}
                spacing={{ xs: 1, sm: 2, md: 4 }}>

                <Grid sx={{ display: { xs: "block", sm: "block" } ,color:"#faa81d"}}>
                  Batch list
                </Grid >
                <Box sx={{ flexGrow: 1, display: { xs: "block", sm: "block" } }}>
                  <Search>
                    <SearchIconWrapper>
                      <SearchIcon sx={{ color: "gray" }} />
                    </SearchIconWrapper>
                    <StyledInputBase
                      sx={{ marginLeft: "40px" }}
                      placeholder="Search"
                      inputProps={{ 'aria-label': 'search' }}
                    />
                  </Search>
                </Box>
                <Box  sx={{ display: { xs: "flex", md: "flex" }, marginRight:"19px !important" }}>
                  <Button onClick={handleNewbatch} variant="contained"
                   sx={{ color: "#FFFFFF", borderColor: "#faa81d",backgroundColor:"#faa81d",':hover': { border: "1px solid #faa81d", backgroundColor: "#faa81d" }}}>+ New Batch </Button>
                </Box>
              </Stack>


              <TableComponent rows={rowsBatch} rowHeader={batchHeader} actions={["edit","delete"]}/>


            </Grid>
            {/* <Grid item xs={8} sm={6} mt={5}>
            <Item>4</Item>
          </Grid> */}
          </Grid>

        </Grid>

      </Grid>

    </Box>

  )
}

export default BatchComponent