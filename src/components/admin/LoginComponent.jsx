import { Grid, Box, Typography, InputLabel, OutlinedInput, InputAdornment, IconButton, Button, ButtonGroup } from '@mui/material'
import React, { useState } from 'react'
import bgimg from "../../assests/bgimg.jpg";
import bulpimg from "../../assests/bulpimg.jpg";
import logo from "../../assests/logo.png";
import { styled } from '@mui/system';
import { Visibility, VisibilityOff } from '@mui/icons-material';
import {Link, useNavigate} from "react-router-dom"
import MentorChPwdModal from '../../atoms/mentorComponents/MentorChPwdModal';

const FirstContainer = styled('div')({
    backgroundImage: `url(${bgimg})`,
    height: "100vh",
    backgroundSize: "100% 100%",
    display: "flex",
    justifyContent: "center",
    alignContent: "center",

})
const Bulpgrid = styled('div')({
    backgroundImage: `url(${bulpimg})`,
    height: "80vh",
    width: "80vw",
    backgroundSize: "100% 100%",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeate",
    borderRadius: "13px",
    marginTop: "5%",
    position: "relative"
})
export function LoginComponent({mentorLogin}) {
    const navigation = useNavigate();
    const [values, setValues] = useState({
        showPassword: false,
        active: true,
        idErr:false,
        pwdErr:false
    });
    const [userdata, setUserData] = useState({userId:"",password:""});
    const[changeModal,setChangeModal]= useState(false)


    const handleClickShowPassword = () => {
        setValues({
            ...values,
            showPassword: !values.showPassword,
        });
    };
    const handleBtnClick = () => {
        debugger
        setValues({ ...values, active: !values.active })
        console.log(userdata);
        if(userdata.userId!=="" && userdata.password!==""){
            setValues({...values,pwdErr:false,idErr:false})  
           mentorLogin===false ? navigation("/home/batch"):navigation("/mentor/dashboard")
            
        }else{
            setValues({...values,pwdErr:true,idErr:true})
        }
    }
    
    return (
        <FirstContainer>
              {changeModal && (
        <MentorChPwdModal open={changeModal} setOpen={setChangeModal}/>
      )}
            <Bulpgrid >
                <Grid container>
                    <Grid item md={8} sm={8} xs={12} sx={{ color: "white", }}>
                        <Grid sx={{ fontFamily: "Agrandir Variable", fontSize: "52px", position: "absolute", bottom: "0", margin: "4%", display: "flex", justifyContent: "left", flexDirection: 'column', textAlign: "left" }}>
                            <Grid>  Good things on</Grid>
                            <Grid >
                                your way!
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item md={4} xs={12} sm={4} sx={{ backgroundColor: "#4e4e4e", height: "80vh", width: "100%", borderRadius: " 0px 13px 13px 0px" }}>
                        <Box sx={{ mx: 4, my: 8, display: "flex", flexDirection: "column", alignItems: "center" }}>
                            <Grid >
                                <img src={logo} alt="TechnoElevate" width="250px" />
                            </Grid>
                            <Grid>
                                <Typography component="h1" variant="h4" color="#FFAA17" sx={{ font: "normal normal normal 35px/120px Abril Fatface;", }}>Login</Typography></Grid>
                            <Box>
                                <InputLabel sx={{ color: "#FFFFFF", textAlign: "left", fontSize: "12px" }}>Employee ID</InputLabel>
                                <OutlinedInput
                                    id="name"
                                    type="text"
                                    error={values.idErr}
                                    helperText={values.idErr ? "Please enter userName":""}
                                    value={userdata.userId}
                                    onChange={(event) => { setUserData({ ...userdata, userId: event.target.value }) }}
                                    label=""
                                    size="small"
                                    fullWidth
                                    sx={{ backgroundColor: "white", borderRadius: "8px" }}
                                />
                                <InputLabel sx={{ color: "#FFFFFF", textAlign: "left", fontSize: "12px", mt: "5%" }}>Password</InputLabel>
                                <OutlinedInput
                                    id="outlined-adornment-password"
                                    type={values.showPassword ? 'text' : 'password'}
                                    error={values.pwdErr}
                                    helperText={values.pwdErr ? "Please enter Password":""}
                                    value={userdata.password}
                                    onChange={(event) => { setUserData({ ...userdata, password: event.target.value }) }}
                                    endAdornment={
                                        <InputAdornment position="end">
                                            <IconButton
                                                aria-label="toggle password visibility"
                                                onClick={handleClickShowPassword}
                                                edge="end"
                                            >
                                                {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                            </IconButton>
                                        </InputAdornment>
                                    }
                                    label=""
                                    size="small"
                                    fullWidth
                                    sx={{ backgroundColor: "white", borderRadius: "8px" }}
                                />
                            </Box>
                            <Grid mt={2} sx={{ width: "70%" }}>
                                <ButtonGroup fullWidth>
                                    <Button onClick={handleBtnClick} variant="outlined" fullWidth sx={{ border: "1px solid #FAA81D", color: "#FFFFFF", backgroundColor: values.active ? "#FAA81D" : "", ':hover': { border: "1px solid #FAA81D", backgroundColor: values.active ? "#FAA81D" : "", } }}>LOGIN</Button>
                                    <Button onClick={handleBtnClick} variant="outlined" fullWidth sx={{ border: "1px solid #FAA81D", color: "#FFFFFF", backgroundColor: values.active ? "" : "#FAA81D", ':hover': { border: "1px solid #FAA81D", backgroundColor: values.active ? "" : "#FAA81D", } }} >CANCEL</Button>
                                </ButtonGroup>

                            </Grid>
                            <Grid sx={{ marginTop: "20%", width: "100%" }}>
                               {mentorLogin && ( <Typography onClick={()=>setChangeModal(true)} sx={{display:"flex",justifyContent:"center",color:"#FFFFFF",':hover': {color:"#FAA81D"}}}>Change Password</Typography>)}
                                <hr style={{ width: "100%" }} />
                                <Grid sx={{ display: "flex", justifyContent: "space-between", padding: "5px" }}>
                                    <Typography sx={{ fontSize: "9px", color: "white" }}>Copyright © 2018 Aleercio.com</Typography>
                                    <Typography sx={{ fontSize: "9px", color: "white" }}>Terms & Conditions | Privacy policy</Typography>
                                </Grid>
                            </Grid>
                        </Box>
                    </Grid>
                </Grid>
            </Bulpgrid>

        </FirstContainer>

    )
}
