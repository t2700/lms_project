export const rowsBatch = [
    {
      "name": "Giraffa camelopardalis",
      "type": "mammal",
      "description": "Tall, with brown spots, lives in Savanna",
     
    },
    {
      "name": "Loxodonta africana",
      "type": "mammal",
      "description": "Big, grey, with big ears, smart",
      
    },
    {
      "name": "Trioceros jacksonii",
      "type": "reptile",
      "description": "Green, changes color, lives in 'East Africa'",
      
    }
  ]
  
  export const batchHeader = ["No.", "Batch ID","Mentor Name", "Technologies","Start Date","End Date", "Status","Action" ];
  