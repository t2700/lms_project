import { Avatar, Box, Breadcrumbs, Grid, Link, List, ListItem, ListItemAvatar, ListItemText, Paper, Stack, TextField, Typography } from '@mui/material'
import React from 'react'
import Navbar from '../../atoms/Navbar'
import ResponsiveDrawer from '../../atoms/ResponsiveDrawer'
import { styled } from '@mui/material/styles';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import PersonIcon from '@mui/icons-material/Person';
import { LineChartComponent } from '../../atoms/LineChartComponent';

export default function MentorEmpListEmployee({ mentorLogin }) {
    const breadcrumbs = [
        <Link
            underline="hover"
            key="2"
            color="inherit"
        >
            Home
        </Link>,
        <Link
            underline="hover"
            key="2"
            color="inherit"
        >
            Batch name
        </Link>,
        <Link
            underline="hover"
            key="2"
            color="inherit"
        >
            Employee list
        </Link>,
        <Typography key="3" color="text.primary">
            Employee
        </Typography>,
    ];

    const Item = styled(Paper)(({ theme }) => ({
        backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
        ...theme.typography.body2,
        padding: theme.spacing(1),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }));


    return (

        <Box sx={{ width: '100%', bgcolor: "#f9f9f9" }}>
            <Grid container rowSpacing={5}>
                <Grid item xs={12} md={12} lg={12} sm={12}>

                    <Navbar mentorLogin={mentorLogin} />

                </Grid>
                <Grid item xs={3} sm={1.5} lg={1} md={1} xl={1} mt={5}>

                    <ResponsiveDrawer mentorLogin={mentorLogin} />
                </Grid>
                <Grid item xs={9} sm={10} lg={11} md={11} xl={11} mt={5}>
                    <Breadcrumbs
                        separator={<NavigateNextIcon fontSize="small" />}
                        aria-label="breadcrumb"
                    >
                        {breadcrumbs}
                    </Breadcrumbs>
                    <Grid container direction="row" rowSpacing={5} columnSpacing={{ xs: 1, sm: 2, md: 2 }}>
                        <Grid item xs={12} sm={12} bgcolor="#FFFFFF" sx={{ margin: "5% 2% 0% 0%" }}>
                           <Stack direction={{ xs: 'row', sm: 'row' }} sx={{ paddingBottom: "20px" }}
                                spacing={{ xs: 1, sm: 2, md: 4 }}>

                            <Box sx={{ flexGrow: 1, display: { xs: "block", sm: "block" } }}>
                            <Grid container sx={{ display: "flex", justifyContent: "space-between" }}>
                                <Grid item md={6} sm={6} xs={12}>
                                    <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
                                        <ListItem>
                                            <ListItemAvatar>
                                                <Avatar sx={{ width: 90, height: 90, backgroundColor: "#FFFFFF", border: "1px solid lightgray" }}>
                                                    <PersonIcon sx={{ fontSize: "80px", color: "gray" }} />
                                                </Avatar>
                                            </ListItemAvatar>
                                            <ListItemText primary={
                                                <Typography
                                                    sx={{ display: 'inline', font: "normal normal bold 54px/74px Open Sans;", marginLeft: "4%" }}
                                                    variant="h4"
                                                    color="#0D0D0D"
                                                >
                                                   Sharan
                                                </Typography>
                                            } secondary={
                                                <Typography
                                                    sx={{ marginLeft: "4%" }}
                                                    variant="h6"
                                                    color="#0D0D0D"
                                                >
                                                    #15422252
                                                </Typography>
                                            }
                                            />
                                        </ListItem>
                                    </List>
                                </Grid>
                                <Grid item md={4}>

                                </Grid>
                                <Grid item md={2}sm={6} xs={12}>
                                    <span style={{ fontSize: "17px" }}> No. of class attended :</span> <span style={{ color: "#0D0D0D", fontSize: "20px", fontWeight: "bold" }}>20/25</span>
                                </Grid>

                                <Grid item md={12} sm={12} xs={12} sx={{ width: "100%" }}>

                                    <fieldset>
                                        <legend>Mock Rating</legend>
                                       <LineChartComponent/>
                                    </fieldset>
                                </Grid>

                            </Grid>
                            </Box>
                            </Stack> 
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Box>

    )
}

