import { Box, Breadcrumbs, Grid, Stack, Typography, } from '@mui/material'
import React, { useState } from 'react'
import Navbar from '../../atoms/Navbar'
import ResponsiveDrawer from '../../atoms/ResponsiveDrawer'
import DropDownComponent from '../../atoms/admincomponents/DropDownComponent';
import { PieChartComponent } from '../../atoms/PieChartComponent';
import { BarChart } from '@mui/icons-material';
import { BarChartComponent } from '../../atoms/BarChartComponent';


function DashboardComponent({ mentorLogin }) {
  const [age, setAge] = useState('');

  //drop down
  let singleDropData = [
    { label: "Batch name", value: "one" },
    { label: "Batch Two", value: "two" },
    { label: "Batch Three", value: "three" },
  ]

  const handleSingleDrop = (event) => {
    setAge(event.target.value);
  };

  //gender data
  let gender_label = ['Male', 'Female'];
  let gender_data = [300, 100];
  let genderbgColor = ['#086288', '#ED9232'];

  //Batch performane
  let batch_label = ['Excellent', 'Good', "Above Average", "Average", "Below Average"];
  let batch_data = [200, 50, 150, 100, 150];
  let batchbgColor = ['#39BB5C', '#E40347', "#EA8604", "#E4D402", "#2DB5EE"];

  //year of passing
  let year_label = ['2022', '2021', "2020", "2019", "2018", "2017", "2016"];
  let year_data = [0.5, 0.5, 1, 1, 2, 2, 2];
  let year_bgColor = ["#02C5E9"];

  //year of passing
  let exp_label = ['Fresher', '1Yr', "2Yrs", "3Yrs", "4Yrs"];
  let exp_data = [6, 4, 3, 1, 2];
  let exp_bgColor = ["#02C5E9"];
  return (

    <Stack sx={{ width: '100%', bgcolor: "#f9f9f9" }}>
      <Grid container rowSpacing={5}>
        <Grid item xs={12} md={12} lg={12} sm={12}>
          <Navbar mentorLogin={mentorLogin} />
        </Grid>
        <Grid item xs={3} sm={1.5} lg={1} md={1} xl={1} mt={5}>
          <ResponsiveDrawer mentorLogin={mentorLogin} />
        </Grid>
        <Grid item xs={9} sm={10} lg={11} md={11} xl={11} mt={5} bgcolor="#FFFFFF">
          <Box p={2} sx={{ display: "flex", justifyContent: "end", }}>
            <DropDownComponent
              size="small"
              singleDropData={singleDropData}
              handleSingleDrop={handleSingleDrop}
              age={age}
              setAge={setAge}
              width={150}
              label="Batch name"
            />
          </Box>
          <Grid container gap={1}>
            <Grid item md={3} lg={4} xs={6} sm={12} height=" 250px"
              sx={{ boxShadow: "0px 0px 4px #00000014", border: "1px solid #182C5224", borderRadius: "7px", opacity: "1", background: "#FFFFFF 0% 0% no-repeat padding-box" }}>
              <Typography variant='h6'>Gender</Typography>
              <Grid p={3}>
                <PieChartComponent cutout="70%" gender_label={gender_label} gender_data={gender_data} genderbgColor={genderbgColor} />
              </Grid>
            </Grid>
            <Grid item md={4} lg={4} xs={6} sm={12} height=" 250px"
              sx={{ boxShadow: "0px 0px 4px #00000014", border: "1px solid #182C5224", borderRadius: "7px", opacity: "1", background: "#FFFFFF 0% 0% no-repeat padding-box" }}>
              <Typography variant='h6'>Year of passing</Typography>
              <Grid p={5}>
                <BarChartComponent bar_label={year_label} bar_data={year_data} barbgColor={year_bgColor} indexAxis='y' axis='y' />
              </Grid>
            </Grid>
            <Grid item md={4} lg={3.5} xs={6} sm={12} height=" 250px"
              sx={{ boxShadow: "0px 0px 4px #00000014", border: "1px solid #182C5224", borderRadius: "7px", opacity: "1", background: "#FFFFFF 0% 0% no-repeat padding-box" }}>
              <Typography variant='h6'>Experience</Typography>
              <Grid p={5}>
                <BarChartComponent bar_label={exp_label} bar_data={exp_data} barbgColor={exp_bgColor} indexAxis='y' axis='y' />
              </Grid>
            </Grid>
            <Grid item md={8} lg={8.07} xs={6} sm={12} height=" 250px"
              sx={{ boxShadow: "0px 0px 4px #00000014", border: "1px solid #182C5224", borderRadius: "7px", opacity: "1", background: "#FFFFFF 0% 0% no-repeat padding-box" }}>
              <Typography variant='h6'>Experience</Typography>
              <Grid p={5}>
                <BarChartComponent bar_label={exp_label} bar_data={exp_data} barbgColor={exp_bgColor} indexAxis='x' axis='x' />
              </Grid>            </Grid>
            <Grid item md={4} lg={3.5} xs={6} sm={12} height=" 250px"
              sx={{ boxShadow: "0px 0px 4px #00000014", border: "1px solid #182C5224", borderRadius: "7px", opacity: "1", background: "#FFFFFF 0% 0% no-repeat padding-box" }}>
              <Typography variant='h6'>Batch Performance</Typography>
              <Grid p={3}>
                <PieChartComponent cutout="0%" gender_label={batch_label} gender_data={batch_data} genderbgColor={batchbgColor} />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Stack>
  )
}

export default DashboardComponent