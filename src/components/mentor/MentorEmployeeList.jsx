import { Box, Breadcrumbs, Button, Chip, Grid, InputBase, Link, Paper, Stack, Typography } from '@mui/material'
import React, { useState, useEffect } from 'react'
import Navbar from '../../atoms/Navbar'
import ResponsiveDrawer from '../../atoms/ResponsiveDrawer'
import { styled } from '@mui/material/styles';
import SearchIcon from '@mui/icons-material/Search';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import MentorTableComponent from '../../atoms/mentorComponents/MentorTableComponent';
import { useNavigate, useParams } from "react-router-dom";
import DropDownComponent from '../../atoms/admincomponents/DropDownComponent';
import MentorEmpCustomTable from '../../atoms/mentorComponents/MentorEmpCustomTable';
import MenuItemComponent from '../../atoms/mentorComponents/MenuItemComponent';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import WarningOutlinedIcon from '@mui/icons-material/WarningOutlined';
import CreateMockModal from '../../atoms/mentorComponents/CreateMockModal';
import MockRatingModal from '../../atoms/mentorComponents/MockRatingModal';
import RequestRejectModal from '../../atoms/admincomponents/RequestRejectModal';


export default function MentorEmployeeList({ mentorLogin }) {
  const [open, setOpen] = useState(false);
  const [mockModalopen, setMockModalopen] = useState(false);
  const [rejectModalopen, setRejectModalopen] = useState(false);
  const [dropValue, setDropValue] = useState('');
  const { id } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    console.log(id)
  }, [])
  const [anchorEl, setAnchorEl] = useState(null);
  const [anchorE2, setAnchorE2] = useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
};

  //drop down
  let singleDropData = [
    { label: "Active", value: "active" },
    { label: "Two", value: "two" },
    { label: "Three", value: "three" },
  ]
  const handleSingleDrop = (event) => {
    setDropValue(event.target.value);
  };
const handleRatingClick=(event)=>{
  setAnchorE2(event.currentTarget);
}
  const breadcrumbs = [
    <Link
      underline="hover"
      key="2"
      color="inherit"
    >
      Home
    </Link>,
    <Link
      underline="hover"
      key="2"
      color="inherit"
    >
      Batch name
    </Link>,
    <Typography key="3" color="text.primary">
      Employee List
    </Typography>,
  ];

  const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }));

  const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: "#BCBCCB",
    '&:hover': {
      backgroundColor: "#BCBCCB",
    },

    margin: "0 auto",
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: "75%",
      width: '25%',
    },
    opacity: "0.5"
  }));
  const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  }));
  const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: "black",
    '& .MuiInputBase-input': {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: '34ch',

      },
      marginLeft: "-2rem"
    },
  }));


  let menuItemdata = [
    {
      title: "Absconded",
      color: "#086288",
      total: ""
    },
    {
      title: "Terminated",
      color: "#EC3E66",
      total: ""
    },
    {
      title: "Active",
      color: "#EC3E66",
      total: ""
    },
  ]
  let menudata2 = [
    {
        title: "Initial Strength",
        color: "#086288",
        total: "100"
    },
    {
        title: "Dropout",
        color: "#EC3E66",
        total: "10"
    },
    {
        title: "Terminated",
        color: "#EC3E66",
        total: "10"
    },
    {
        title: "Absconding",
        color: "#EC3E66",
        total: "10"
    },
    {
        title: "Present Strength",
        color: "#02B91B",
        total: "70"
    }
]
  const getMockRatingsData = ()=>{
    return(<>
      <WarningOutlinedIcon sx={{ fontSize: "40px", color: "#C9A805" }} onMouseOver={handleRatingClick} />
      <MenuItemComponent menudata={menudata2} openmenu={Boolean(anchorE2)} anchorEl={anchorE2} 
      setAnchorEl={setAnchorEl} width="200px !important" handleClose={()=> setAnchorE2(null)} />
      </>
    )
  }
  const onEditEmpClick = (id)=>{
    debugger
    navigate(`/mentor/batch/emplist/employee/${id}`);
  }
  const getStatusData = () => {
    return (
      <>
        <Button
          id="demo-customized-button"
          aria-controls={open ? 'demo-customized-menu' : undefined}
          aria-haspopup="true"
          aria-expanded={open ? 'true' : undefined}
          variant=""
          disableElevation
          onClick={handleClick}
          endIcon={<KeyboardArrowDownIcon />}
        >
          Active
        </Button>
        <MenuItemComponent menudata={menuItemdata} openmenu={Boolean(anchorEl)}
         anchorEl={anchorEl} setAnchorEl={setAnchorEl} width="110px !important"handleClose={()=> setAnchorEl(null)}handleMenuClick={()=>setRejectModalopen(true)} />
      </>
    )
  }
  let dataHeader = [
    { label: "Employee ID", id: "empID" },
    { label: "Employee Name", id: "empName" },
    { label: "MockTaken", id: "mockTaken" },
    { label: "Mockrating", id: "mockRating" },
    { label: "Attendance", id: "attendance" },
    { label: "Status", id: "status" },

  ]
  let empdata = [{
    empID: "15422252",
    empName: "abc",
    status: getStatusData(),
    mockRating: getMockRatingsData(),
    attendance: "In progress",
    mockTaken: "14-07-2022",
  }];

  return (

    <Box sx={{ width: '100%', bgcolor: "#f9f9f9" }}>
       {mockModalopen && (
        <CreateMockModal open={mockModalopen} setOpen={setMockModalopen}/>
      )}
       {rejectModalopen && (
        <RequestRejectModal open={rejectModalopen} setOpen={setRejectModalopen} label="Reason to Change Status"/>
      )}
      
      <Grid container rowSpacing={5}>
        <Grid item xs={12} md={12} lg={12} sm={12}>
          <Navbar mentorLogin={mentorLogin} />
        </Grid>
        <Grid item xs={3} sm={1.5} lg={1} md={1} xl={1} mt={5}>
          <ResponsiveDrawer mentorLogin={mentorLogin} />
        </Grid>
        <Grid item xs={9} sm={10} lg={11} md={11} xl={11} mt={5}>
          <Breadcrumbs
            separator={<NavigateNextIcon fontSize="small" />}
            aria-label="breadcrumb"
          >
            {breadcrumbs}
          </Breadcrumbs>
          <Grid container direction="row" rowSpacing={5} columnSpacing={{ xs: 1, sm: 2, md: 2 }}>
            <Grid item xs={12} sm={12} bgcolor="#FFFFFF" sx={{ margin: "5% 2% 0% 0%" }}>
              <Stack direction={{ xs: 'row', sm: 'row' }} sx={{ paddingBottom: "20px" }}
                spacing={{ xs: 1, sm: 2, md: 4 }}>
                <Grid sx={{ display: { xs: "block", sm: "block" }, color: "#faa81d" }}>
                  Employee list ({id})
                </Grid >
                <Box sx={{ flexGrow: 1, display: { xs: "block", sm: "block" } }}>
                  <Search>
                    <SearchIconWrapper>
                      <SearchIcon sx={{ color: "gray" }} />
                    </SearchIconWrapper>
                    <StyledInputBase
                      sx={{ marginLeft: "40px" }}
                      placeholder="Search"
                      inputProps={{ 'aria-label': 'search' }}
                    />
                  </Search>
                </Box>
                <Box sx={{ display: { xs: "flex", md: "flex" }, }}>
                  <DropDownComponent
                    size="small"
                    singleDropData={singleDropData}
                    handleSingleDrop={handleSingleDrop}
                    age={dropValue}
                    setAge={setDropValue}
                    width={140}
                    placeholder="Download"
                  />   </Box>
                <Box sx={{ display: { xs: "flex", md: "flex" }, marginRight: "19px !important" }}>
                  <Button variant="contained" onClick={()=>setMockModalopen(true)}
                    sx={{ color: "#FFFFFF", borderColor: "#faa81d", backgroundColor: "#faa81d", ':hover': { border: "1px solid #faa81d", backgroundColor: "#faa81d" } }}>Create mock </Button>
                </Box>
              </Stack>
              <MentorEmpCustomTable rows={empdata} rowHeader={dataHeader} empList="true" onEditClick={onEditEmpClick}/>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Box>

  )
}
