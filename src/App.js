import './App.css';
import { LoginComponent } from './components/admin/LoginComponent';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom"
import BatchComponent from './components/admin/BatchComponent';
import { MentorComponent } from './components/admin/MentorComponent';
import { RequestComponent } from './components/admin/RequestComponent';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import DashboardComponent from './components/mentor/DashboardComponent';
import MentorBatchComponent from './components/mentor/MentorBatchComponent';
import MentorEmployeeList from './components/mentor/MentorEmployeeList';
import MentorEmpListEmployee from './components/mentor/MentorEmpListEmployee';

function App() {
 
  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
    <Router>
      <Routes>
        <Route exact path="/" element={<LoginComponent mentorLogin={false}/>} />
       
        <Route exact path="/home/batch" element={<BatchComponent/>} />
        <Route exact path="/home/mentor" element={<MentorComponent/>} />
        <Route exact path="/home/request" element={<RequestComponent/>} />
        <Route exact path="/mentor" element={<LoginComponent mentorLogin={true}/>} />
        <Route exact path="/mentor/dashboard" element={<DashboardComponent mentorLogin={true}/>} />
        <Route exact path="/mentor/batch" element={<MentorBatchComponent mentorLogin={true}/>} />
        <Route exact path="/mentor/batch/emplist/:id" element={<MentorEmployeeList mentorLogin={true}/>} />
        <Route exact path="/mentor/batch/emplist/employee/:id" element={<MentorEmpListEmployee mentorLogin={true}/>} />
      </Routes>
    </Router>
    </LocalizationProvider>
  );
}

export default App;
